import gymnasium as gym

import overloaded

def test_loop():
    env = gym.make("overloaded/Overloaded-v0")
    observation, info = env.reset(seed=42)
    for _ in range(4):
        action = env.action_space.sample()
        observation, reward, terminated, truncated, info = env.step(action)
        print(action, observation, reward)
        # TODO: assert 

        if terminated or truncated:
            observation, info = env.reset()
    env.close()

