import numpy as np

from jobsnow.events import JobEvent
from jobsnow.simulate import SimulatedSystem

def test_simulate():
    rng = np.random.default_rng()
    S = SimulatedSystem(rng, max_nodes=4000)
    skipped_jobs = set()
    for i in range(10):
        assert len(S.events) > 0
        t = S.next_time()
        assert t is not None
        for e in S.advance(rng, (t-S.time)*4):
            # Opportunistic policy - never revisit waiting
            # jobs.
            print(e)
            if e.name == JobEvent.queued:
                if S.avail_nodes >= e.job.nodes:
                    print("starting")
                    n = len(S.events)
                    S.start_job(rng, e.job)
                    assert len(S.events) == n+1
                else:
                    skipped_jobs.add( e.job.jobid )
            elif e.name == JobEvent.canceled:
                if e.job.running == None:
                    skipped_jobs.remove( e.job.jobid )

    pend = S.list_pending()
    assert len(pend) == len(skipped_jobs)
    print( S.show_user_stats() )

    print( S.qstat() )
