from importlib.metadata import version
__version__ = version(__package__)

from gymnasium.envs.registration import register

register(
    id="overloaded/Overloaded-v0",
    entry_point="overloaded.envs:Overloaded",
)

