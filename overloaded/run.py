import gymnasium as gym
from gymnasium.utils import play

import overloaded
from jobsnow.system import calc_user_stats, softmax

def interact():
    env = gym.make("overloaded/Overloaded-v0", render_mode="rgb_array")
    play.play(env,
              keys_to_action=dict([(str(i),i) for i in range(5)]),
              noop=0)
    env.close()

def autopilot():
    env = gym.make("overloaded/Overloaded-v0", render_mode="human")
    observation, info = env.reset(seed=42)
    for _ in range(100):
        action = env.action_space.sample()
        observation, reward, terminated, truncated, info = env.step(action)
        print(env.system.time, action, observation, reward)

        if terminated or truncated:
            observation, info = env.reset()
    order, y, hrs = calc_user_stats(env.system.users, env.system.time)
    print(order)
    print(y)
    print(softmax(y))
    print(hrs)
    env.close()

def run():
    import sys
    argv = sys.argv
    if len(argv) > 1 and argv[1] == "-a":
        autopilot()
    else:
        interact()

if __name__=="__main__":
    run()
