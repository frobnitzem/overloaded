import gymnasium as gym
from gymnasium.utils import play
from gymnasium.wrappers import FlattenObservation

import torch

from jobsnow.system import calc_user_stats, softmax

import overloaded
from overloaded.agent import SchedulerAgent

def train(model : str, device : str, num_episodes : int):
    # Note: can reconfigure / add more job size classes on init.
    env = FlattenObservation( gym.make("overloaded/Overloaded-v0") )
    agent = SchedulerAgent(
        device = device,
        num_classes = env.unwrapped.N,
        learning_rate = 1e-4,
        initial_epsilon = 0.9,
        epsilon_decay = 1.5,
        final_epsilon = 0.05,
        tau = 0.005,
        discount_factor = 0.95,
    )
    if agent.is_saved(model):
        agent.load(model)

    scheduler_steps = 2400*2 # 2 days

    episode_scores = []
    for episode in range(num_episodes):
        # Initialize the environment and get it's state
        state, info = env.reset()
        state = torch.tensor(state, dtype=torch.float32, device=device)\
                     .unsqueeze(0) # cast to a batch of size 1
        agent.reset(state)

        for t in range(scheduler_steps):
            action = agent.get_action(state) # tensor
            observation, reward, terminated, truncated, info = \
                    env.step(action.item())
            next_state = torch.tensor(observation, dtype=torch.float32,
                                      device=device).unsqueeze(0)
            reward = torch.tensor([[reward]], device=device)

            agent.update(action, next_state, reward)

        episode_scores.append(env.unwrapped.score)
        order, y, hrs = calc_user_stats(env.unwrapped.system.users, env.unwrapped.system.time)
        print(f"{episode} {env.unwrapped.score}")
        print(f"min/max/avg wait_length = {y.min()}/{y.max()}/{y.mean()}")
        print(f"min/max/avg node_hrs = {hrs.min()}/{hrs.max()}/{hrs.mean()}")
        #plot_durations()

    agent.save(model)
    print('Complete')
    #plot_durations(show_result=True)
    #plt.ioff()
    #plt.show()

    env.close()

def run():
    import sys
    argv = sys.argv
    assert len(argv) == 2, "Usage: {argv[0]} <model.pth>"
    if False: #torch.cuda.is_available():
        num_episodes = 600
        device = "cuda"
    else:
        num_episodes = 50
        device = "cpu"

    train(argv[1], device, num_episodes)

if __name__=="__main__":
    run()
