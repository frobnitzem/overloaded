from typing import Optional
from pathlib import Path

import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

from torchrl.data.replay_buffers import TensorDictReplayBuffer
from torchrl.data.replay_buffers.storages import LazyMemmapStorage
from torchrl.data.replay_buffers.samplers import SamplerWithoutReplacement
from tensordict import TensorDict

class DQN(nn.Module):
    def __init__(self, n_observations, n_actions):
        super(DQN, self).__init__()
        self.layer1 = nn.Linear(n_observations, 128)
        self.layer2 = nn.Linear(128, 128)
        self.layer3 = nn.Linear(128, n_actions)

    # Called with either one element to determine next action, or a batch
    # during optimization. Returns tensor([[left0exp,right0exp]...]).
    def forward(self, x):
        x = F.relu(self.layer1(x))
        x = F.relu(self.layer2(x))
        return self.layer3(x)

class SchedulerAgent:
    def __init__(
        self,
        device: str,
        num_classes: int,
        learning_rate: float,
        initial_epsilon: float,
        epsilon_decay: float,
        final_epsilon: float,
        tau: float = 0.005,
        discount_factor: float = 0.95,
        seed:Optional[int] = None,
    ):
        self.batch_size = 128
        self.device = device
        self.num_classes = num_classes
        self.tau = tau
        self.discount_factor = discount_factor
        self.epsilon = initial_epsilon
        self.epsilon_decay = epsilon_decay
        self.final_epsilon = final_epsilon

        n_observations = num_classes*3+2
        n_actions = num_classes+1

        self.policy_net = DQN(n_observations, n_actions).to(device)
        self.target_net = DQN(n_observations, n_actions).to(device)
        self.target_net.load_state_dict(self.policy_net.state_dict())

        self.optimizer = optim.AdamW(self.policy_net.parameters(),
                                     lr=learning_rate,
                                     amsgrad=True)

        self.memory = TensorDictReplayBuffer(
            storage = LazyMemmapStorage(10000),
            sampler = SamplerWithoutReplacement(),
            batch_size = self.batch_size,
        )

        self.np_random = np.random.default_rng(seed=seed)

    def is_saved(self, prefix : str) -> bool:
        return Path(prefix + "_policy.pth").exists() \
               and Path(prefix + "_target.pth").exists()

    def load(self, prefix : str) -> None:
        self.policy_net.load_state_dict(torch.load(prefix + "_policy.pth"))
        self.target_net.load_state_dict(torch.load(prefix + "_target.pth"))

    def save(self, prefix : str) -> None:
        torch.save(self.policy_net.state_dict(), prefix + "_policy.pth")
        torch.save(self.target_net.state_dict(), prefix + "_target.pth")

    def reset(self, state, seed:Optional[int] = None):
        self.np_random = np.random.default_rng(seed=seed)
        self.state = state
        #self.memory = ReplayMemory(10000)

    def get_action(self,
                   state: torch.Tensor) -> torch.Tensor:
        """
        Returns a batch of actions with probability (1 - epsilon)
        otherwise a random action with probability epsilon to ensure exploration.
        """
        # with probability epsilon return a random action to explore the environment
        if self.np_random.random() < self.epsilon:
            act = int((self.num_classes+1)*self.np_random.random())
            return torch.tensor([[act]]*len(state),
                                device=self.device,
                                dtype=torch.long)

        # with probability (1 - epsilon) act greedily (exploit)
        #return int(np.argmax(self.q_values[state]))
        with torch.no_grad():
            # t.max(1) will return the largest column value of each row.
            # second column on max result is index of where max element was
            # found, so we pick action with the larger expected reward.
            return self.policy_net(state).max(1).indices.view(1, 1)

    def decay_epsilon(self):
        self.epsilon = max(self.final_epsilon, self.epsilon/self.epsilon_decay)

    def update(self, action, next_state, reward):
        # Store the transition in memory
        #self.memory.extend([ torch.tensor([[self.state, action, next_state, reward]) ])
        self.memory.extend(TensorDict({"state": self.state,
                                       "action": action,
                                       "next_state":  next_state,
                                       "reward": reward}, [1]))
        #self.memory.push(self.state, action, next_state, reward)
        # Move to the next state
        self.state = next_state

        self.optimize_model()

        # Soft update of the target network's weights
        # θ′ ← τ θ + (1 −τ )θ′
        target_net_state_dict = self.target_net.state_dict()
        policy_net_state_dict = self.policy_net.state_dict()
        for key in policy_net_state_dict:
            target_net_state_dict[key] = \
                       policy_net_state_dict[key]*self.tau \
                     + target_net_state_dict[key]*(1.0-self.tau)
        self.target_net.load_state_dict(target_net_state_dict)

    def optimize_model(self):
        if len(self.memory) < self.batch_size:
            return None
        batch = self.memory.sample(self.batch_size)

        # Concatenate the batch elements from the memory sample.
        next_states = batch["next_state"]
        state_batch = batch["state"]
        action_batch = batch["action"]
        reward_batch = batch["reward"]

        # Compute Q(s_t, a) - the model computes Q(s_t), then we select the
        # columns of actions taken. These are the actions which would've been taken
        # for each batch state according to policy_net
        state_action_values = self.policy_net(state_batch).gather(1, action_batch)

        # Compute V(s_{t+1}) for all next states.
        # Expected values of actions for next_states are computed based
        # on the "older" target_net; selecting their best reward
        # with max(1).values
        with torch.no_grad():
            next_state_values = self.target_net(next_states).max(1).values \
                                    .view(action_batch.shape)
        #print(next_state_values.shape)
        # Compute the expected Q values
        expected_state_action_values = reward_batch \
                                     + next_state_values*self.discount_factor
        #print(next_state_values.shape, expected_state_action_values.shape)

        # Compute Huber loss
        criterion = nn.SmoothL1Loss()
        loss = criterion(state_action_values, expected_state_action_values)

        # Optimize the model
        self.optimizer.zero_grad()
        loss.backward()
        # In-place gradient clipping
        nn.utils.clip_grad_value_(self.policy_net.parameters(), 100)
        self.optimizer.step()
