from typing import Optional, Any, Tuple
import bisect

import gymnasium as gym
from gymnasium import spaces

import pygame

import numpy as np

from jobsnow.events import JobEvent
from jobsnow.simulate import SimulatedSystem

window_height, window_width = 700, 500
#  
# y  ---
# ^  | |
# |  ---
# |  #==
#  ---> x
#
# Transformation: (x,y) ~> (col, row)
#  - global row = (window_height-1)*(1.0 - global_y)
#  - global col = global_x*(window_width-1)
def global_x(x):
    return x*(window_width-1)
def global_y(y):
    return (window_height-1)*(1.0 - y)

class Overloaded(gym.Env):
    """ The Overladed environment contains a list of jobs that are ready
        to run (in the pending state).  The player can choose which
        job to start on the cluster at any time.  But watch out.
        If all the nodes are occupied, then the queue will become overladed!

        The objective of this game is to run as many jobs as possible,
        utilize all resouces as much as possible, and keep the users
        happy (by keeping wait times low).

        As the users wait, they become more impatient, and may begin to
        misbehave - gaming the queue to push their jobs to the top.

        The observation space will contain information about
        pending jobs, as well as information about the state of the cluster.

        Information about pending jobs comes in the form of a vector,
        where each entry represents a particular job class, grouped by nodes.
        For example, using classes = [1, 10, 400, 1000], we group jobs into
         - class 1: 1-9 nodes
         - class 2: 10-399 nodes
         - class 3: 400-1000 nodes

        The last number is treated differently, since it determines max_nodes.
        
        Several job properties are included as separate observations:

        - number of jobs in the bucket
        - priority of the highest priority job in that bucket
          (the next one to be selected)
        - number of jobs in the bucket with nonzero priority
        - ...

        There is an additional observation for the number of total
        and unused nodes on the machine.

        The action space is simple - pick one of the N buckets from
        which to launch the next job.  An additional "0" action will
        do nothing.
    """
    metadata = {"render_modes": ["human", "rgb_array"], "render_fps": 40}

    def __init__(self, render_mode=None,
                 classes = [1, 92, 922, 2765, 4608],
                 nusers : int = 100) -> None:
        self.dt = 0.01
        self.A = 10
        self.B = 5

        self.render_mode = render_mode
        self.classes = sorted(classes)
        assert classes[0] == 1, "First job class must contain 1 node."
        self.N = len(classes)-1 # number of classes
        self.classes[-1] += 1 # fix last range
        self.score = 0

        self.nusers = nusers
        self.system = SimulatedSystem(self.np_random,
                                      max_nodes=classes[-1],
                                      nuser=nusers)
        self.observation_space = spaces.Dict({
                    "pending": spaces.Box(low=0, high=65535,
                                          shape=(self.N,), dtype=np.int32),
                    "priority": spaces.Box(low=0.0, high=1.0,
                                           shape=(self.N,), dtype=np.float32),
                    "nonzero": spaces.Box(low=0, high=65535,
                                          shape=(self.N,), dtype=np.int32),
                    "nodes": spaces.Box(0,
                                        self.max_nodes,
                                        shape=(2,),
                                        dtype=np.int32)
                    # nodes[0] = unoccupied "free" nodes
                    # nodes[1] = total nodes in the system
                }
            )
        # 0 = nothing, 1+ = start job from class i+1 (1 = class 0, etc.)
        self.action_space = spaces.Discrete(self.N+1)

        self.window : Optional[pygame.Surface]    = None
        self.clock  : Optional[pygame.time.Clock] = None
        self.font   : Optional[pygame.font.Font]  = None

    @property
    def max_nodes(self):
        return self.system.max_nodes

    def _get_obs(self):
        N = self.N

        pending = np.zeros(N, dtype=np.int32)
        priority = np.zeros(N, dtype=np.float32)
        nonzero = np.zeros(N, dtype=np.int32)
        for job in self.system.list_pending():
            cl = self.to_jobclass(job.nodes)-1
            pending[cl] += 1
            priority[cl] += job.x
            nonzero[cl] += job.y > 0.0
        return { "pending":  pending,
                 "priority": priority,
                 "nonzero":  nonzero,
                 "nodes":    np.array([self.system.avail_nodes,
                                       self.system.max_nodes],
                                      dtype=np.int32)
                    # nodes[0] = unoccupied "free" nodes
                    # nodes[1] = total nodes in the system
               }

    def _get_info(self):
        # TODO: some indication of current score
        # - system utilization
        # - max job wait time
        # - balance of hrs between projects
        return {}

    # Transformation for machine state area:
    #  - x in [0,max_nodes] ~> global_x in [0.05, 0.95]
    #  - y in [0,1] ~> global_y in [0.15, min(0.25, nodes_height/window_height)]
    def nodes_x(self, x):
        return global_x( x/self.max_nodes*(0.95-0.05) + 0.05 )

    def nodes_y(self, y):
        nodes_height = 20
        ht = min(0.25, nodes_height/window_height)
        return global_y( y*(ht - 0.15) + 0.15 )

    # Transformation for pending job area:
    #  - x in [0.5,max_nodes+0.5] ~> global_x in [0.05, 0.95]
    #  - y in [0,1] ~> global_y in [0.3, 0.95]
    def job_x(self, x):
        return global_x( (x-0.5)/self.max_nodes*(0.95-0.05) + 0.05 )

    def job_y(self, y, scale=0.96):
        top = -np.log1p( -scale )
        ey = 1.0 + np.log1p( (y-1.0)*scale )/top

        return global_y( 0.95 - ey*(0.95-0.3) )

    def reset(self, seed : Optional[int] = None, options=None
              ) -> Tuple[Any, dict[str, Any]]:
        # We need the following line to seed self.np_random
        super().reset(seed=seed)
        self.score = 0

        self.system = SimulatedSystem(self.np_random,
                                      max_nodes=self.max_nodes,
                                      nuser=self.nusers)

        observation = self._get_obs()
        info = self._get_info()

        if self.render_mode == "human":
            self._render_frame()

        return observation, info

    def render(self) -> Optional[np.ndarray]: #type: ignore[override]
        if self.render_mode == "rgb_array":
            return self._render_frame()
        return None

    def _render_at(self, canvas, s, x, y, color=(255,255,255)) -> None:
        assert self.font is not None
        text = self.font.render(str(s), True,
                                color, (0,0,0))
        rect = text.get_rect()
        rect.center = (global_x(x), global_y(y))
        canvas.blit(text, rect)

    def _render_frame(self) -> Optional[np.ndarray]:
        if self.window is None and self.render_mode in ["human", "rgb_array"]:
            pygame.init()
            pygame.display.init()
            pygame.display.set_caption('Overloaded')
            self.window = pygame.display.set_mode((window_width,window_height))
        if self.clock is None and self.render_mode == "human":
            self.clock = pygame.time.Clock()

        if self.font is None:
            self.font = pygame.font.Font('freesansbold.ttf', 32)
        canvas = pygame.Surface((window_width, window_height))
        canvas.fill((0, 0, 0))

        row0 = self.job_y(0.0)
        row1 = self.job_y(1.0)
        for n in self.classes:
            x = self.job_x(n-0.5)
            pygame.draw.line(canvas, (255,255,255),
                             (x, row0), (x, row1), width=3)
        col0 = self.job_x(0.5)
        col1 = self.job_x(self.max_nodes+0.5)
        for row in [row0, row1]:
            pygame.draw.line(canvas, (255,255,255),
                (col0, row), (col1, row), width=3)

        # area for displaying machine state
        row0 = self.nodes_y(0.0)
        row1 = self.nodes_y(1.0)
        #obs  = self._get_obs()
        free = self.system.avail_nodes # obs["nodes"][0]
        occ  = self.system.max_nodes - free

        # border for node space
        border = 4
        col0 = self.nodes_x(0.0)
        col1 = self.nodes_x(self.max_nodes)
        pygame.draw.rect(canvas, (255, 255, 255),
                         pygame.Rect(col0-border,
                                     row0-border,
                                     2*border+col1-col0,
                                     2*border+row1-row0)
                        )
        # forest green for occupied nodes
        pygame.draw.rect(canvas, (30, 200, 30),
                         pygame.Rect(col0, row0,
                                     self.nodes_x(occ)-col0, row1-row0)
                        )
        # dark grey for unoccupied nodes
        pygame.draw.rect(canvas, (10, 10, 10),
                         pygame.Rect(self.nodes_x(occ), row0,
                                     col1-self.nodes_x(occ), row1-row0)
                        )
        # Use some lines to mark off currently running job sizes.
        nocc = [job.nodes for job in self.system.list_running()]
        nocc.sort()
        n_single = bisect.bisect_right(nocc, 1) # number of single-node jobs
        k = 0
        # draw vertical separators between job sizes
        for n in reversed(nocc[n_single:]):
            k += n
            x = self.nodes_x(k)
            pygame.draw.line(canvas, (120,120,120),
                             (x, row0), (x, row1), width=1)
        if n_single > 0: # Draw some horizontal hatches for single-node jobs.
            x = self.nodes_x(k)
            y = self.nodes_x(k+n_single)
            for i in range(7):
                row = row0 + (i+0.5)/7 * (row1-row0)
                pygame.draw.line(canvas, (120,120,120),
                                 (x, row), (y, row), width=1)
            k += n_single
        assert k == occ, f"running_nodes={k} != occ={occ}"

        sum_x = 0.0
        for job in self.system.list_pending():
            #if job.x > 0:
            #    print(job.x)
            sz = 4.0 + 6.0*(job.user/len(self.system.users))
            pygame.draw.circle(canvas, (255, 255, 255, 100),
                               (self.nodes_x(job.nodes), self.job_y(job.x)), sz)
            sum_x += job.x

        self._render_at(canvas, int(self.score), 0.75, 0.225)
        hrs = int(self.system.time)
        mins = int(60 * (self.system.time-hrs))
        self._render_at(canvas, f"{hrs%24:02d}:{mins:02d}",  0.2, 0.225,
                         color=(30,30,200))
        self._render_at(canvas, int(sum_x*1000), 0.45, 0.225, color=(200,30,30))

        if self.render_mode == "human":
            # The following line copies our drawings from `canvas` to the visible window
            self.window.blit(canvas, canvas.get_rect()) # type: ignore[union-attr]
            pygame.event.pump()
            pygame.display.update()

            # We need to ensure that human-rendering occurs at the predefined framerate.
            # The following line will automatically add a delay to keep the framerate stable.
            self.clock.tick(self.metadata["render_fps"]) # type: ignore[union-attr]
        else:  # rgb_array
            return np.transpose(
                np.array(pygame.surfarray.pixels3d(canvas)), axes=(1, 0, 2)
            ) # type: ignore[union-attr]
        return None

    def to_jobclass(self, nodes):
        cl = bisect.bisect_right(self.classes, nodes)
        assert cl > 0 and cl <= self.N
        return cl

    def get_pending_job(self, jobclass):
        best = None
        jobid = np.inf
        prio = 0.0
        for job in self.system.list_pending():
            if int(self.to_jobclass(job.nodes)) != jobclass:
                continue
            #if self.system.avail_nodes < job.nodes:
            #    continue
            if job.x > prio or ((job.x == prio) and job.jobid < jobid):
                best = job
                jobid = job.jobid
                prio = job.x
        return best

    def step(self, action):
        # action is {0,1,...,N}
        if action > 0:
            # start the highest prio. job from this jobclass
            job = self.get_pending_job(action)
            if job is not None and job.nodes <= self.system.avail_nodes:
                self.system.start_job(self.np_random, job)

        # advance time
        for e in self.system.advance(self.np_random,self.dt):
            pass

        sum_x = sum(job.x for job in self.system.list_pending())
        self.score += self.dt * (self.A * (1.0 - self.system.avail_nodes
                                                 / self.system.max_nodes)
                                -self.B * sum_x )

        # get observation and info.
        observation = self._get_obs()
        info = self._get_info()

        if self.render_mode == "human":
            self._render_frame()

        terminated = False
        reward = 0.0 # TODO: compute from throughput (nodes occupied, etc.)
        return observation, reward, terminated, False, info

    def close(self):
        if self.window is not None:
            pygame.display.quit()
            pygame.quit()
            self.window = None

