Overloaded
==========

A game to manually schedule jobs on a supercomputer.

Jobs fall from the top.  Drop them onto the machine
before they reach the player.

Installation
------------

Install standalone:

    pip install git+https://gitlab.com/frobnitzem/overloaded.git

Install in editable mode (for developers):

    python3 -m venv venv
    . venv/bin/activate
    pip install -e .

Running
-------

Play mode:

    overloaded

Auto-action mode:

    overloaded -a

Model Training:

    overloaded_train

Hacking
-------

Contributions and ideas are welcome.  Open an issue or
merge request!

There's a [youtube video series](https://www.youtube.com/watch?v=-askh1ULZNA)
documenting the creation of this package.
